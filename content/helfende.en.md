---
title: Volunteering
draft: false
---
**Join the FSR VI – Shape Your University Experience!**

Do you want to participate in university life, contribute to exciting projects, and meet new people? Then join us and become a part of the FSR VI!

**Your benefits as a helper:**

- **Access to the Ini:** You'll have your own access to our FSR room, a cozy space for studying or relaxing – including **free use of the coffee machine**!

- **First insights into university politics:** Learn how decisions are made at the university and take part in shaping your student experience.

- **Contribute to projects:** Whether it’s events, workshops, or initiatives, you’ll have the chance to participate in projects and bring your ideas to life.

If you're ready to make a difference and join a motivated community, reach out to us! We look forward to having you join us!
