---
title: Impressum
draft: false
---
<address>
Berliner Hochschule für Technik Berlin
Fachschaftsrat des Fachbereich VI
Luxemburger Straße 10
13353 Berlin
</address>

Du erreichst uns per E-Mail unter [fsr6@studis-bht.de](mailto:fsr6@studis-bht.de) oder sporadisch telefonisch unter der [+49 30 4504 2318](tel:+49-30-4504-2318) erreichen. Leider können wir derzeit nicht zurückrufen.

Der Fachschaftsrat VI ist ein Organ der Studierendenschaft der [Berliner Hochschule für Technik](https://www.bht-berlin.de), einer rechtsfähigen Teilkörperschaft der Hochschule. Die Studierendenschaft wird vertreten durch den [AStA](https://asta.studis-bht.de).

![](/img/memes/we-do-not-test-on-animals-we-test-in-production.svg)

## Bildnachweise

{{% copyright %}}

[Sticker: We do not test on animals, we test in production](https://blog.kohler.is/sticker-we-do-not-test-on-animals-we-test-in-production/): [CC-BY-SA-NC 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
