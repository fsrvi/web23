---
title: Notfallinformationen für Studis
draft: false
---
Der FSR VI wird hier auf dieser Webseite aktuelle Informationen zu dem Ransomware-Angriff zusammentragen.

(Stand: 04.03.2024)

## FAQ

### Allgemein

#### Was ist passiert?

Am Dienstag, den 20. Februar 2024, wurde die Hochschule mit einer Ransomware angegriffen. Dadurch wurden viele virtuelle Maschinen im Hochschulrechenzentrum verschlüsselt. Da auf diesen Servern Dienste wie die Webseite, Polli, Moodle und vieles mehr liefen, sind diese Dienste gerade nicht erreichbar. Auch wurden Daten aus der Hochschule ausgeleitet.

#### Was nun?

Die Hochschule arbeitet mit externen IT-Unternehmen zusammen, um das Ausmaß zu erfassen und festzustellen, welche Systeme infiziert wurden.

Die Hochschule besitzt laut eigener Aussage für alle Systeme Backups, ihr müsst also keine Angst haben, dass eure Daten verloren sind! Da jetzt aber erst einmal herausgefunden werden muss, seit wann die Systeme infiziert sind, wird es einige Zeit dauern, bis die Backups eingespielt werden können und die Dienste wieder verfügbar sind.

#### Was solltet ihr tun?

Erst einmal solltet ihr Ruhe bewahren.

Die Situation wird noch analysiert und es können vermutlich alle Daten wiederhergestellt werden. Viel Panik und Spekulation helfen gerade nicht, gültige Informationen zu verbreiten.

Die Hochschule hat begonnen, eine Notfallwebseite aufzubauen, um Informationen verteilen zu können. Die Seite findet ihr unter [www.bht-notfallseite.de](https://www.bht-notfallseite.de).

Da die Angreifer möglicherweise Zugriff auf euer Hochschul-Passwort erhalten haben, solltet ihr bei allen anderen Diensten, wo ihr das gleiche Passwort verwendet habt, dieses schnellstmöglich ändern.

#### Was hat das für Auswirkungen auf euer Studium?

Wir haben einen weiteren [Discord-Server (Einladungslink)](https://discord-fb.fsrvi.de) eingerichtet, damit ihr mit Lehrenden in Kontakt treten könnt, um die Unterlagen zu erhalten und eure Unterlagen mit anderen Studis teilen könnt. Auch das Dekanat des FB 6 wird auf diesem Discord erreichbar sein. Einige Lehrende haben bereits angefangen ihre Unterlagen dort bereitzustellen.

Außerdem gibt es weiterhin unseren [Fachschafts-Discord-Server](https://discord.fsrvi.de) nur für Studierende. Wir halten euch aber auch hier weiterhin auf dem Laufenden.

#### Wie erreiche ich jemanden in der Hochschule?

Wer Unterlagen bei der Hochschule einreichen muss, macht das am besten persönlich bei den Ansprechpartnern oder per Post.

Auch gibt es bei der Verdi-Betriebsgruppe eine [Telefonliste](https://verdi-bht.de/telefonliste-bht/).

Wenn ihr Lehrende erreichen wollt oder Ansprechpartner sucht, dann könnt ihr über die [Wayback Machine ein Archiv der BHT-Webseite](https://web.archive.org/web/20240219230249/https://www.bht-berlin.de/) durchsuchen. Bitte beachte aber, dass in den Semesterferien viele Lehrende Urlaub haben, und teilweise auch deshalb nicht erreichbar sind.

Der Studien-Info-Service ist jetzt Montag bis Freitag von 8 bis 17 Uhr geöffnet, ihr könnt wichtige Unterlagen auch dort abgeben. Außerdem gibt es auch eine Hotline für Anfragen unter der Telefonnummer [030 4504 2020](tel:+49-30-4504-2020), erreichbar von 8 bis 17 Uhr.

Das Dekanat ist Montag bis Freitag von 8:30 bis 13:30 Uhr unter der Telefonnummer [030 4504 5860](tel:+49-30-4504-5860) sowie von 9:00 bis 14:00 Uhr vor Ort im Raum B 131 (Haus Gauß, 1. OG) erreichbar. Abschlussarbeiten und weitere Dokumente können dort abgegeben werden.

Außerhalb der Öffnungszeiten können Abschlussarbeiten über den Fristbriefkasten der Poststelle abgegeben werden, dieser befindet sich von außen zugänglich im Haus Bauwesen und ist über die Vorfahrt an der Limburger Str. erreichbar.

#### Wie erreiche ich den AStA?

Der AStA ist wie gewohnt telefonisch oder per Mail erreichbar.

Alle Infos erhaltet ihr auf der [AStA-Webseite](https://asta.studis-bht.de/).

#### Was ist mit meinem Semesterticket?

Für das Semesterticket ab April wird noch nach einer Lösung gesucht, da es digital sein sollte.

Das Upgrade zum Deutschland-Ticket ist gerade nicht möglich, wer ein laufendes Abo hat, sollte das schriftlich kündigen, da das Anmeldeportal gerade nicht nutzbar ist.

#### Was ist mit den Klausuren?

Die Noten aus dem ersten Prüfungszeitraum sind sicher.

Durch die Hochschule wurde folgende Regelung für den zweiten Prüfungszeitraum festgelegt:

> 1. Die im regulären 2. Prüfungszeitraum des Wintersemesters 2023/24 (16. bis 31.03.2024, siehe § 19 Abs. 3 RSPO2016) erbrachten Leistungen „1,0“ bis „4,0“ bzw. „mit Erfolg“ werden gewertet. Für „5,0“ bzw. „ohne Erfolg“ wird von den Lehrkräften eine Nichtteilnahme eingetragen. Ausnahme: Die Bewertung „5,0“ bzw. „ohne Erfolg“ werden verbucht, wenn sie aus einem Täuschungsversuch heraus resultieren.
> 1. Für Studierende, die im regulären 2. Prüfungszeitraum nicht teilgenommen haben bzw. einen Fehlversuch hatten, wird bei Bedarf im Verlauf des Sommersemesters 2024 eine Nachprüfung angeboten. Diese wird frühestens vier Wochen nach der Wiederherstellung von Moodle angesetzt. Die Bewertungen („1,0“ bis „5,0“, „mit Erfolg“, „ohne Erfolg“) werden statt der Nichtteilnahmen in die Notenliste des 2. Prüfungszeitraumes des Wintersemesters 2023/24 eingetragen.
> 1. Studierenden im 4. Belegsemester, die wegen Nichtteilnahme ein „endgültig nicht bestanden“ (ggf. auch in einem Folgesemester) erhalten, wird auf Antrag die Belegung für das Wintersemester 2023/24 von der Studienverwaltung storniert.

#### Wie komme ich an meine Immatrikulationsbescheinigung?

Ein Informationsschreiben der Hochschulpräsidentin zur aktuellen Situation zur Einreichung bei Behörden und weiteren Stellen ist [hier](https://bht-notfallseite.de/wp-content/uploads/2024/02/Informationsschreiben-der-Praesdentin.pdf) von der Notfallseite verfügbar.

Wie ihr an eine Immatrikulationsbescheinigung kommt, ist noch in Klärung.

#### Was ist mit meiner Abschlussarbeit?

Die Abschlussarbeiten können aufgrund der Situation im FB 6 verlängert werden. Dafür muss ein formloser Antrag im Dekanat abgegeben werden.

Abschlussarbeiten können „klassisch“ gedruckt und gebunden oder als Datei auf einem USB-Stick, im Dekanat abgegeben werden. Lediglich Deckblatt und Erklärung zur Selbstständigkeit sollten in jedem Fall ausgedruckt werden.

#### Was ist mit der Rückmeldung und BAföG?

Der AStA hat hierzu die [Informationen](https://asta.studis-bht.de/hilfestellung-sicherheitsvorfall/) zusammengetragen und ist auch Ansprechpartner für die Themen.

#### Wie steht’s um SHK Stellen?

Die Studis die eine SHK Stelle bekommen sollen oder eine verlängern möchten, wird es Unterlagen im Dekanat geben, die ausgefüllt werden müssen.

### Praxisphase

#### Was ist mit Praktika?

Zu Fragen zum Thema Praktikum wendet euch bitte an den Praktikumsbeauftragten für euren Studiengang.

Wir werden demnächst Kontaktmöglichkeiten veröffentlichen.

#### Wo können wir die unterschriebenen Praxisverträge abgeben?

Am besten wie früher direkt beim Praxisbeauftragten.

Mit den anderen Praxisbeauftragten müssen individuelle Termine vereinbart werden.

#### Wie werden wir kontaktiert, ob unser Vertrag seitens der Hochschule angenommen wurde?

Wenn ein Praxisbeauftragter unterschrieben hat (Mixdorff, Voß oder Loewel für Medieninformatik und TI bspw.), dann ist das Praktikum akzeptiert.

#### Falls das Problem nicht bis zum Semesterstart gelöst werden kann: Wie können wir uns in den Kurs für den Praktikumsbericht eintragen?

Ein Einschreiben in den Kurs für die Praxisphase ist bei MI und TI nicht notwendig.

### Praxisphase Druck- und Medientechnik

Informationen zur Praxisphase in der Druck- und Medientechnik sind auf der [Studiengangs-Webseite](https://dmt-berlin.de/praktikum/) abrufbar.

## Dokumente

Wir haben die aktuellen Dokumente, die ihr brauchen könntet aus unserem Archiv geholt und eingescannt.

Ihr könnt euch die Dokumente auch im Dekanat ausgedruckt abholen.

- [Scan – Zulassung Abschlussarbeit](/img/DokumenteFB6Scans/scan_zulassungabschluss.pdf)
- [Scan – Zulassung Abschlussarbeit – Anlage 1](/img/DokumenteFB6Scans/scan_zulassungabschluss_anlage1.pdf)
- [Scan – Zulassung Abschlussarbeit – Anlage 2](/img/DokumenteFB6Scans/scan_zulassungabschluss_anlage2.pdf)
- [Scan – Praktikumsinfos](/img/DokumenteFB6Scans/scan_praktikuminfos.pdf)
- [Scan – Praktikumsvertrag – deutsch](/img/DokumenteFB6Scans/scan_praktikumsvertrag.pdf)
- [Scan – Praktikumsvertrag – englisch](/img/DokumenteFB6Scans/scan_praktikumsvertrag_english.pdf)
- [Scan – Laufzettel Abschlusszeugnis](/img/DokumenteFB6Scans/scan_laufzettelabschlusszeugniss.pdf)

## Kontakte zu Lehrenden

- Jan-Hendrik Carstens: Carstens_Jan-Hendrik ät web.de
- Michael Dattner: michael.dattner ät gmail.com
- Christian Forler: cforler ät posteo.de
- Edzard Höfig: info ät edzard.net
- Angelika Hönemann: angelika.hoenemann ät uni-osnabrueck.de
- Amy Siu: q5s4e32rv ät mozmail.com
- Simone Strippgen: strippgenbht ät gmail.com
- Florian Süßl: florian ät suessl.de
- Henrik Tramberend: bht ät tramberend.de
- Steffen Wagner: steffen.wagner.bht.berlin ät gmail.com

Lehrende, die ihre E-Mail-Adresse hier aufführen lassen wollen, können eine E-Mail an [fsr6@studis-bht.de](fsr6@studis-bht.de) senden. Wenn die E-Mail-Adresse nicht im Internet stehen soll, kann ein Dienst wie bspw. [Firefox Relay](https://relay.firefox.com/) genutzt werde.
