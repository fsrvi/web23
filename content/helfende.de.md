---
title: Helfende
draft: false
---
**Werde Mitglied – Gestalte deine Hochschule mit!**

Du möchtest dich aktiv ins Uni-Leben einbringen, spannende Projekte mitgestalten und dabei auch noch nette Leute kennenlernen? Dann werde Mitglied im Fachschaftsrat VI!

**Deine Benefits als Mitglied:**

- **Zugang zum Ini:** Mit eigenem Zugang zu unserem Ini-Raum hast du jederzeit eine gemütliche Rückzugsmöglichkeit zum Lernen oder Entspannen – inklusive **freier Nutzung der Kaffeemaschine**!

- **Erste Einblicke in die Hochschul-Politik:** Du erfährst, wie Entscheidungen an der Hochschule getroffen werden und kannst an der Gestaltung deines Studienalltags mitwirken.

- **Mitwirkung an Projekten:** Ob Veranstaltungen, Workshops oder Initiativen – du kannst aktiv an Projekten teilnehmen und eigene Ideen einbringen.

Wenn du Lust hast, etwas zu bewegen und Teil einer engagierten Gemeinschaft zu werden, dann melde dich bei uns! Wir freuen uns auf dich!
