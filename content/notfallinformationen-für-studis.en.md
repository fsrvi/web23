---
title: Emergency Information for Students
draft: false
---
The FSR VI will gather current information on the ransomware attack on this website.

(As of: 04.03.2024)

## FAQ

### General information

#### What happened?

On Tuesday, the 20 February 2024, the university was attacked with ransomware. As a result, many virtual machines in the university data center were encrypted. Since services such as the website, Polli, Moodle, and many more were running on these servers, these services are currently not accessible.

Data from the university was also extracted.

#### What now?

The university is collaborating with external IT companies to assess the extent of the attack and determine which systems were infected.

According to their own statements, the university has backups for all systems, so you don't have to worry about losing your data! However, since it needs to be determined when the systems were infected, it will take some time before the backups can be restored and the services become available again.

#### What should you do?

First, you should remain calm.

The situation is still being analyzed, and it is likely that all data can be restored. Much panic and speculation do not help to spread valid information.

The university has started to set up an emergency website to distribute information, you can find it at [www.bht-notfallseite.de](https://www.bht-notfallseite.de).

As the attackers may have gained access to your university password, you should change it as quickly as possible for all other services where you have used the same password.

#### What are the implications for your studies?

We have set up a new [Discord server (Invitation link)](https://discord-fb.fsrvi.de) so that you can contact lecturers to obtain the materials and share your materials with other students. The Dekanat will be reachable through the Server. Some teaching staff are already sharing their course material through the server.

Our existing students-only [FSR VI-Discord-Server](https://discord.fsrvi.de) is still available. We will keep you up to date here as well.

#### How do I reach someone at the university?

If you need to submit documents to the university, it is best to do so in person with the contact persons or by mail.

There is also a [telephone number list ](https://verdi-bht.de/telefonliste-bht/)provided by Verdi.

If you want to reach lecturers or are looking for contact persons, you can search the [BHT website through the Wayback Machine](https://web.archive.org/web/20240219230249/https://www.bht-berlin.de/). Note that in the lecture-free period many lecturers are on vacation and therefore not available to contact.

The Student Info Service is now open Monday to Friday from 8 a.m. to 5 p.m., and you can also submit important documents there. And there is also a hotline for inquiries: Tel. [030-4504-2020](tel:+49-30-4504-2020) from 8 a.m. to 5 p.m.

The Dekanat (office of the dean) of faculty 6 can be reached from Monday to Friday from 8:30 am to 3:30 pm by phone ([030 4504 5860](tel:+49-30-4504-5860)) or from 9:00 am to 2:00 pm in person in room B131 (Gauß Building, 1st floor).

Outside business hours, theses and other documents can be submitted through the deadline mailbox outside of Bauwesen Building at the driveway on Limburger Str.

#### How do I contact the Student Council (AStA)?

The Student Council (AStA) can be reached as usual by phone or email.

All information can be found at [asta.studis-bht.de](https://asta.studis-bht.de).

#### What about my semester ticket?

A solution for the semester ticket starting in April is being worked on since it is only going to be available in digital form.

The upgrade to a Deutschland-Ticket is currently not possible, if you have a running subscription, it should be cancelled in written form since the login portal is currently not usable.

#### What about the exams?

The grades from the first examination period are safe.

The university has established the following rules for the second examination period:

1. Acreediations graded with “1.0” to “4.0” or “passed” (successfully completed) achieved in the regular 2nd examination period of the winter semester 2023/24 (16 to 31.03.2024, see §19 para. 3 RSPO2016) will be assessed. For “5.0” or “failed”, a non-participation is entered by the teaching staff. Exception: A grade of “5.0” or “unsuccessful” is recorded if it results from an attempt to cheat.
1. For students who have not taken part in the regular 2nd examination period or who have failed, a re-examination will be offered during the summer semester 2024 if required. This will be scheduled at the earliest four weeks after Moodle is restored. The grades (“1.0” to “5.0”, “passed”, “failed”) will be recorded in the grade list of the 2nd examination period of the winter semester 2023/24 instead of the failures.
1. Students in the 4th enrollment who receive a “finally failed” (possibly also in a subsequent semester) due to non-participation will have their enrollment for the winter semester 2023/24 canceled by the Student Administration Office upon request.

#### Additional Information

Please note, some additional information is currently only available on the [German version](/notfallinformationen-für-studis/) of this site.

## Documents

We have collected and scanned useful documents from our archives for you.

They can also be picked up in the dean's office (Dekanat) in printed form.

- [Scan – Registration for the final examination](/img/DokumenteFB6Scans/scan_zulassungabschluss.pdf)

- [Scan – Registration for the final examination – Annex 1](/img/DokumenteFB6Scans/scan_zulassungabschluss_anlage1.pdf)

- [Scan – Registration for the final examination – Annex 2](/img/DokumenteFB6Scans/scan_zulassungabschluss_anlage2.pdf)

- [Scan – Information on Internships](/img/DokumenteFB6Scans/scan_praktikuminfos.pdf)

- [Scan – Internship contract – German](/img/DokumenteFB6Scans/scan_praktikumsvertrag.pdf)

- [Scan – Internship contract – English](/img/DokumenteFB6Scans/scan_praktikumsvertrag_english.pdf)

- [Scan – “Laufzettel” for graduation certificate](/img/DokumenteFB6Scans/scan_laufzettelabschlusszeugniss.pdf)


## Contacts of lecturers

- Jan-Hendrik Carstens: Carstens_Jan-Hendrik ät web.de
- Michael Dattner: michael.dattner ät gmail.com
- Christian Forler: cforler ät posteo.de
- Edzard Höfig: info ät edzard.net
- Angelika Hönemann: angelika.hoenemann ät uni-osnabrueck.de
- Amy Siu: q5s4e32rv ät mozmail.com
- Simone Strippgen: strippgenbht ät gmail.com
- Florian Süßl: florian ät suessl.de
- Henrik Tramberend: bht ät tramberend.de
- Steffen Wagner: steffen.wagner.bht.berlin ät gmail.com
