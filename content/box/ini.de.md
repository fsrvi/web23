---
title: INI-Raum
weight: 4
draft: false
type: ""
class: ""
---
Der {{% abbr Initiativ-Raum %}}INI-Raum{{% /abbr %}} ist der Raum der Fachschaft. Hier kannst du dich mit Kommiliton*innen austauschen, Arbeiten, oder auch einfach im Hochschulalltag mal verschnaufen. Außerdem gibt es hier frischen Kaffee und kalte Getränke.

Den INI-Raum, B 030, findest du im Erdgeschoss von Haus Gauß neben dem mittleren Treppenhaus.
