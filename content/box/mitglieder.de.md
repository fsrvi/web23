---
title: Der Fachschaftsrat
weight: 5
draft: false
type: ""
class: box--full box-lg--half
---
Der FSR wird jedes Jahr im Wintersemester von allen Studierenden des Fachbereichs gewählt. Er unterstützt die Studierenden, stellt den INI-Raum bereit und führt Veranstaltungen wie die Erstsemestereinführung und andere Feiern durch.

{{< people "mitglieder" >}}
