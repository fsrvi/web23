---
title: Wilkommen
weight: 1
draft: false
type: ""
class: ""
---
Alle Studierenden eines Fachbereiches bilden an der BHT eine eigene Fachschaft. Geleitet wird die Fachschaft von ihrem Fachschaftsrat (FSR).

Dem Fachschaftsrat obliegt u.a. die Koordinierung der Betreuung des INI-Raums sowie der „Lernhilfensammlung“. Desweiteren kann der Fachschaftsrat zusätzliche Aufgaben wahrnehmen. So organisiert er die Erstsemestereinführung im Fachbereich.

Er ist Anlaufstelle für alle Probleme, die die Studierenden während ihres Studiums mit der Hochschule haben.
