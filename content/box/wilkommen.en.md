---
title: Welcome
weight: 1
---

All students in a faculty form their own student body (“Fachschaft”) at the BHT. The student council is led by its faculty student council (Fachschaftsrat, FSR).

Among other things, the FSR is responsible for coordinating the supervision of the INI room and the "learning aid collection". Furthermore, the FSR can take on additional tasks. For example, it organizes the first semester orientation in the faculty.

It is the point of contact for all problems that students have with the university during their studies.
