---
title: Emergency information for students
weight: 8
type: ""
class: ""
---

Current information on the ransomeware attack, with important information for students.

[Link to the emergency information](/en/notfallinformationen-f%C3%BCr-studis)
