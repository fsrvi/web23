---
title: INI room
weight: 4
class: ""
---
The {{% abbr "Initiative Room" %}}INI Room{{% /abbr %}} is the faculty student body’s room. Here you can exchange ideas with fellow students, work or simply take a break from everyday university life. Fresh coffee and cold drinks are also available here.

You will find the INI room, B 030, on the first floor of Haus Gauß next to the center staircase.
