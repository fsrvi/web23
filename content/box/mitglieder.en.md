---
title: The Fachschaftsrat
weight: 5
---

The FSR is elected every year in the winter semester by all students in the faculty. It supports the students, provides the INI room and organizes events such as the first semester induction and other celebrations.

{{< people "mitglieder" >}}
