---
title: Notfallinformationen für Studis
weight: 8
draft: false
type: ""
class: ""
---

Aktuelle Informationen zum Ransomeware Angriff, mit wichtigen Informationen für Studis.

[Link zu den Notfallinformationen](/notfallinformationen-f%C3%BCr-studis)
