---
title: Legal
---
<address>
Berliner Hochschule für Technik Berlin
Fachschaftsrat des Fachbereich VI
Luxemburger Straße 10
13353 Berlin
</address>

You can reach us at [fsr6@studis-bht.de](mailto:fsr6@studis-bht.de) or sporadically at [+49 30 4504 2318](tel:+49-30-4504-2318). Unfortunately, we are currently unable to call you back.

The Fachschaftsrat VI is an organ of the student body of the [Berliner Hochschule für Technik](https://www.bht-berlin.de/en/), a corporation under public law that is part of the university. The student body is represented by the [AStA](https://asta.studis-bht.de/en/).

![](/img/memes/we-do-not-test-on-animals-we-test-in-production.svg)
## Image Credits

{{% copyright %}}


[Sticker: We do not test on animals, we test in production](https://blog.kohler.is/sticker-we-do-not-test-on-animals-we-test-in-production/): [CC-BY-SA-NC 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

